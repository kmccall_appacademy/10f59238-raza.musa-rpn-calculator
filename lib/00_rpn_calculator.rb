# require "byebug"
class RPNCalculator
  def initialize
    @array = []
  end

  def push(num)
    @array << num
  end

  def plus
    calc(:+)
  end

  def value
    @array[-1]
  end

  def minus
    calc(:-)
  end

  def divide
    calc(:/)
  end

  def times
    calc(:*)
  end

  def tokens(str)
    symbols = "+-/*"
    array = []
    str = str.split.join("")
      str.each_char do |char|
        if symbols.include?(char)
          array << char.to_sym
        else
          array << char.to_i
        end
      end
      array
    end

    def evaluate(string)
    array = tokens(string)
    symbols = [:+, :-, :/, :*]
    until array.length == 1
      array.each_index do |idx| 
        if symbols.include?(array[idx]) && array[idx] == :*
        answer = (array[idx-2].to_f * array[idx-1].to_f).to_i
        array[idx-1] = answer
        array.delete(array[idx-2])
        array.delete(array[idx-1])
        elsif symbols.include?(array[idx]) && array[idx] == :+
        answer = (array[idx-2].to_f + array[idx-1].to_f).to_i
        array[idx-1] = answer
        array.delete(array[idx-2])
        array.delete(array[idx-1])
        elsif symbols.include?(array[idx]) && array[idx] == :/
        answer = (array[idx-2].to_f / array[idx-1].to_f)
        array[idx-1] = answer
        array.delete(array[idx-2])
        array.delete(array[idx-1])
        elsif symbols.include?(array[idx]) && array[idx] == :-
        answer = (array[idx-2].to_f - array[idx-1].to_f).to_i
        array[idx-1] = answer
        array.delete(array[idx-2])
        array.delete(array[idx-1])
        end
     end
   end
  array[0]
  end


  private
  def calc(symbol)
    raise "calculator is empty" if @array.length < 2
    second_val = @array.pop
    first_val = @array.pop
  case symbol
  when :+
    @array << first_val + second_val
  when :*
    @array << first_val * second_val
  when :/
    answer = first_val.fdiv(second_val)
    @array << answer
  when :-
    subtract = first_val - second_val
    @array << subtract
  end
  end
end
